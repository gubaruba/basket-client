<?php
/**
 * Class BasketController
 *
 * @author Volodymyr Lalykin
 */
namespace controllers;

use models\Basket;
use models\Item;

require(__DIR__ . '/../models/Basket.php');
require(__DIR__ . '/../models/Item.php');

/**
 * Class for handling basket related requests.
 *
 * @package controllers
 * @author Volodymyr Lalykin
 */
class BasketController extends Controller
{
    /**
     * Initializes menu with common items.
     */
    public function __construct()
    {
        parent::__construct();
        $this->menu->addItem(['title' => 'Basket List', 'url' => '', 'active' => 'index']);
        $this->menu->addItem(['title' => 'Create Basket', 'url' => 'basket/create', 'active' => 'create']);
    }

    /**
     * Shows basket list.
     */
    public function actionIndex()
    {
        $this->title = 'Baskets';

        $response = $this->client->get(\App::config('apiVersion') . '/baskets');
        $baskets = json_decode($response->getBody());

        $this->render($this->id . '/index', ['baskets' => $baskets]);
    }

    /**
     * Shows a particular basket details.
     *
     * @param integer $id basket id
     */
    public function actionView($id)
    {
        try {
            $response = $this->client->get(\App::config('apiVersion') . '/baskets/' . $id . '/items');
            $basket = new Basket();
            $basket->populate(json_decode($response->getBody()));
            $this->title = 'Basket ' . $basket->name;
            $this->menu->addItem(['title' => 'Edit Basket', 'url' => 'basket/' . $basket->id . '/update', 'active' => 'edit']);
            $this->render($this->id . '/view', ['basket' => $basket]);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $this->handleError($e);
        }


    }

    /**
     * Creates a new basket.
     */
    public function actionCreate()
    {
        $this->title = 'Create Basket';

        $basket = new Basket();
        if (isset($_POST['basket'])) {
            $basket->populate($_POST['basket']);
            try {
                $this->client->request('POST', \App::config('apiVersion') . '/baskets', [
                    'form_params' => $basket->getFormParams()
                ]);
                $this->redirect(\App::config('baseUrl'));
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                $response = json_decode($e->getResponse()->getBody()->getContents());
                $basket->errors = $response->message;
            }
        }
        $this->render($this->id . '/create', ['basket' => $basket]);
    }

    /**
     * Updates basket details and content by id.
     *
     * @param integer $id basket id
     */
    public function actionUpdate($id)
    {
        $response = $this->client->get(\App::config('apiVersion') . '/baskets/' . $id . '/items');

        $basket = new Basket();
        $data = json_decode($response->getBody());
        $basket->populate($data);

        $this->title = 'Update ' . $basket->name;
        $this->menu->addItem(['title' => 'View Basket', 'url' => 'basket/' . $basket->id, 'active' => 'view']);

        if (isset($_POST['basket'])) {
            $basket->populate($_POST['basket']);
            try {
                $this->client->request('PUT', \App::config('apiVersion') . '/baskets/' . $basket->id, [
                    'form_params' => $basket->getFormParams()
                ]);
                $this->redirect(\App::config('baseUrl') . '/basket/' . $basket->id);
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                $response = json_decode($e->getResponse()->getBody()->getContents());

                if ($response->message == 'Overloaded') {
                    $basket->setError('capacity', 'The basket will be overloaded with existing items');
                } else {
                    $basket->errors = $response->message;
                }
                $basket->populate($data);
            }
        }

        $items = [];

        if (isset($_POST['item'])) {
            $formParams = [];
            foreach ($_POST['item'] as $k => $item) {
                $items[$k] = new Item();
                $items[$k]->populate($item);
                $formParams['item'][$k] = json_encode($item);
            }

            if ($items) {
                try {
                    $this->client->request('POST', \App::config('apiVersion') . '/baskets/' . $basket->id . '/items', [
                        'form_params' => $formParams
                    ]);
                    $this->redirect(\App::config('baseUrl') . '/basket/' . $basket->id . '/update#items-tab');
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    $response = json_decode($e->getResponse()->getBody()->getContents());
                    if ($response->message == 'Overloaded') {
                        $basket->setError('content', 'The basket will be overloaded. Please check it for heavy items.');
                    } else {
                        foreach ($response->message as $k => $error) {
                            if (isset($items[$k])) {
                                $items[$k]->errors = $error;
                            }
                        }
                    }
                }
            }
        }
        $this->render($this->id . '/update', ['basket' => $basket, 'items' => $items]);
    }

    /**
     * Deletes a particular basket. It could be deleted only via POST request.
     *
     * @param integer $id
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        if (!$this->checkIsPostRequest()) {
            throw new \Exception('Bad request', 400);
        }

        try {
            $this->client->delete(\App::config('apiVersion') . '/baskets/' . $id);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            //
        }
    }

    /**
     * Deletes items from the basket. It could be deleted only via POST request.
     *
     * @throws \Exception
     */
    public function actionDeleteItem()
    {
        if (!$this->checkIsPostRequest() || !isset($_POST['basketId']) || !isset($_POST['itemId'])) {
            throw new \Exception('Bad request', 400);
        }

        try {
            $this->client->delete(\App::config('apiVersion') . '/baskets/' . $_POST['basketId'] . '/items/' . $_POST['itemId']);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            //
        }
    }

    /**
     * Handles an AJAX request for adding one more item fields block.
     *
     * @param integer $index index of the item fields block
     * @throws \Exception
     */
    public function actionAddItem($index)
    {
        if (!$this->checkIsPostRequest()) {
            throw new \Exception('Bad request', 400);
        }
        echo $this->renderView($this->id . '/item', ['index' => $index]);
    }
}