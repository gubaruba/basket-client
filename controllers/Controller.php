<?php
/**
 * Class Controller
 *
 * @author Volodymyr Lalykin
 */
namespace controllers;

use GuzzleHttp\Client;
use helpers\Menu;

/**
 * Parent controller class.
 *
 * @package controllers
 * @author Volodymyr Lalykin
 */
class Controller
{
    /**
     * @var string controller id
     */
    public $id;

    /**
     * @var string action id
     */
    public $actionId;

    /**
     * @var Client HTTP-client for sending requests
     */
    public $client;

    /**
     * @var string page title
     */
    public $title;

    /**
     * @var Menu menu
     */
    public $menu;

    public function __construct()
    {
        $this->menu = new Menu();

        $this->client = new Client([
            'base_uri' => \App::config('apiUrl') . '/' . \App::config('apiVersion'),
            'timeout' => 2
        ]);
    }

    /**
     * Renders view into layout. If couldn't find requested file exception will be thrown.
     *
     * @param string $relativeFilepath path relative to views directory
     * @param array $data array of variables which will be used for rendering
     * @throws \Exception
     */
    public function render($relativeFilepath, $data = [])
    {
        $content = $this->renderView($relativeFilepath, $data);
        require_once(__DIR__ . '/../views/common/layout.php');
    }

    /**
     * Renders subview into the current view. If couldn't find requested file exception will be thrown.
     *
     * @param string $relativeFilepath path relative to views directory
     * @param array $data array of variables which will be used for rendering
     * @throws \Exception
     */
    public function renderSubview($relativeFilepath, $data = [])
    {
        $filepath = __DIR__ . '/../views/' . $relativeFilepath . '.php';

        if (!is_file($filepath)) {
            throw new \Exception('The requested view not found');
        }

        if (is_array($data)) {
            extract($data);
        }

        require($filepath);
    }

    /**
     * Return view as a string. If couldn't find requested file exception will be thrown.
     *
     * @param string $relativeFilepath path relative to views directory
     * @param array $data array of variables which will be used for rendering
     * @throws \Exception
     * @return string
     */
    public function renderView($relativeFilepath, $data = [])
    {
        $filepath = __DIR__ . '/../views/' . $relativeFilepath . '.php';

        if (!is_file($filepath)) {
            throw new \Exception('The requested view not found: ' . $filepath);
        }

        if (is_array($data)) {
            extract($data);
        }

        ob_start();
        ob_implicit_flush(false);
        require($filepath);
        return ob_get_clean();
    }

    /**
     * Redirects browser to passed url.
     *
     * @param string $url
     */
    public function redirect($url)
    {
        header('Location: ' . $url, true, 302);
    }

    /**
     * Checks if current request has POST type
     *
     * @return boolean
     */
    public function checkIsPostRequest()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    /**
     * Handles exception. Generates error page.
     *
     * @param \Exception $e
     */
    public function handleError($e)
    {
        $this->title = 'Error ' . $e->getResponse()->getStatusCode();
        $content = $this->renderView('common/error', ['message' => $e->getResponse()->getReasonPhrase()]);
        require_once(__DIR__ . '/../views/common/layout.php');
        exit();
    }
}