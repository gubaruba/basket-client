<?php
/**
 * Class Basket
 *
 * @author Volodymyr Lalykin
 */
namespace models;

/**
 * Class contains operations with baskets.
 *
 * @package models
 * @author Volodymyr Lalykin
 */
class Basket extends Model
{
    /**
     * @var integer basket id
     */
    public $id;

    /**
     * @var string basket name
     */
    public $name;

    /**
     * @var integer basket capacity
     */
    public $capacity;

    /**
     * @var integer current summary weight of item placed in the basket
     */
    public $weight;

    /**
     * @var string comma separated item id list
     */
    public $content;

    /**
     * @var Item[] items placed in the basket
     */
    public $items = [];

    /**
     * Returns basket class name
     *
     * @return string
     */
    public static function getClassName()
    {
        return __CLASS__;
    }

    /**
     * Array of params for API request.
     *
     * @return array
     */
    public function getFormParams()
    {
        return [
            'name' => $this->name,
            'capacity' => $this->capacity,
        ];
    }

    /**
     * Returns the CSS class name for weight progress bar.
     *
     * @return string
     */
    public function getProgressClass()
    {
        $className = 'progress-bar-';

        $percent = ($this->weight * 100) / $this->capacity;
        if ($percent <= 25) {
            return $className . 'info';
        } elseif ($percent <= 50) {
            return $className . 'success';
        } elseif ($percent <= 75) {
            return $className . 'warning';
        } else {
            return $className . 'danger';
        }
    }
}