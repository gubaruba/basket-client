<?php
/**
 * Class Model
 *
 * @author Volodymyr Lalykin
 */
namespace models;

/**
 * Parent class for all models.
 *
 * @package models
 * @author Volodymyr Lalykin
 */
class Model
{
    /**
     * @var string[] array of validation errors.
     */
    public $errors = [];

    /**
     * Returns model class name. If called directly from this class exception will be thrown.
     *
     * @throws \Exception
     */
    public static function getClassName()
    {
        throw new \Exception('Must be called from inherited class');
    }

    /**
     * Populates model with data.
     *
     * @param array $data
     * @throws \Exception
     */
    public function populate($data)
    {
        if (!is_array($data)) {
            $data = (array) $data;
        }

        foreach ($data as $attribute => $value) {
            if (property_exists(static::getClassName(), $attribute)) {
                $this->$attribute = $value;
            }
        }
    }

    /**
     * Returns true if model has errors. Method checks errors by specific attribute if optional parameter attribute is
     * passed.
     *
     * @param string|null $attribute optional attribute parameter. Null by default.
     * @return boolean
     */
    public function hasErrors($attribute = null)
    {
        if ($attribute) {
            if (!property_exists(static::getClassName(), $attribute)) {
                return false;
            }
            return !empty($this->errors->$attribute);
        }
        return !empty($this->errors);
    }

    /**
     * Returns validation errors. Method returns errors by specific attribute if optional parameter attribute is
     * passed.
     *
     * @param string|null $attribute optional attribute parameter. Null by default.
     * @return array
     */
    public function getErrors($attribute = null)
    {
        if ($attribute && property_exists(static::getClassName(), $attribute)) {

            if ($this->hasErrors($attribute)) {
                return $this->errors->$attribute;
            }
            return [];
        }
        return (array) $this->errors;
    }

    /**
     * Sets custom error message to the attribute
     *
     * @param string $attribute
     * @param string $message
     * @throws \Exception
     */
    public function setError($attribute, $message)
    {
        if (property_exists(static::getClassName(), $attribute)) {
            if (!$this->errors) {
                $this->errors = new \stdClass();
            }
            if (!isset($this->errors->$attribute)) {
                $this->errors->$attribute = [];
            }
            array_push($this->errors->$attribute, $message);
        }
    }
}