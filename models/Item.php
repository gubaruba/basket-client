<?php
/**
 * Class Item
 *
 * @author Volodymyr Lalykin
 */
namespace models;

/**
 * Class contains operations on basket items.
 *
 * @package models
 * @author Volodymyr Lalykin
 */
class Item extends Model
{
    /**
     * @var integer item id
     */
    public $id;

    /**
     * @var string item type
     */
    public $type;

    /**
     * @var integer item weight
     */
    public $weight;

    /**
     * Returns item class name.
     *
     * @return string
     */
    public static function getClassName()
    {
        return __CLASS__;
    }
}