<?php
/**
 * @var string[] $errors
 * @var controllers\Controller $this
 */
?>
<?php if ($errors): ?>
    <?php foreach ($errors as $error): ?>
        <span class="text-danger"><?= $error ?></span><br>
    <?php endforeach ?>
<?php endif ?>
