<?php
/**
 * @var controllers\Controller $this
 */
?>
<?php if ($this->menu->getItems()): ?>
    <ul class="nav nav-pills nav-stacked">
        <?php foreach($this->menu->getItems() as $item): ?>
            <?php if (isset($item['title'])): ?>
                <li role="presentation" class="<?= (isset($item['active']) && $this->actionId == $item['active']) ? 'active' : '' ?>"><a href="<?= App::config('baseUrl') . '/' . (isset($item['url']) ? $item['url'] : '') ?>"><?= $item['title'] ?></a></li>
            <?php endif ?>
        <?php endforeach ?>
    </ul>
<?php endif ?>
