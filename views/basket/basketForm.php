<?php
use helpers\StringHelper;

/**
 * @var models\Basket $basket
 * @var controllers\BasketController $this
 */
?>

<h2>Basket</h2>

<div class="row">
    <div class="col-md-12">
        <form method="post" class="form-horizontal">
            <div class="form-group <?= $basket->hasErrors('name') ? 'has-error' : '' ?>">
                <label for="name" class="col-sm-2">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="basket[name]" value="<?= StringHelper::encode($basket->name) ?>">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <?php $this->renderSubview('common/errors', ['errors' => $basket->getErrors('name')]) ?>
                </div>

            </div>
            <div class="form-group  <?= $basket->hasErrors('capacity') ? 'has-error' : '' ?>">
                <label for="capacity" class="col-sm-2">Capacity</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="capacity" name="basket[capacity]" value="<?= StringHelper::encode($basket->capacity) ?>">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <?php $this->renderSubview('common/errors', ['errors' => $basket->getErrors('capacity')]) ?>
                </div>
            </div>

            <input type="hidden" name="tabId" value="0">

            <button type="submit" class="btn btn-default">Save</button>
        </form>
    </div>
</div>