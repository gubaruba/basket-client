<?php
use helpers\StringHelper;
/**
 * @var models\Basket $basket
 * @var controllers\BasketController $this
 * @var integer $tabId
 * @var models\Item[] $items
 */
?>

<h2>Weight <span id="weight"><?= StringHelper::encode($basket->weight) ?></span>/<span id="capacity"><?= StringHelper::encode($basket->capacity) ?></span></h2>

<div class="progress">
    <div class="progress-bar <?= $basket->getProgressClass() ?>" role="progressbar" style="width: <?= ($basket->weight * 100) / $basket->capacity ?>%"></div>
</div>

<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#basket-tab" aria-controls="basket-tab" role="tab" data-toggle="tab">Basket</a></li>
    <li role="presentation"><a href="#items-tab" aria-controls="items-tab" role="tab" data-toggle="tab">Items</a></li>
</ul>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="basket-tab">
        <?php $this->renderSubview($this->id . '/basketForm', ['basket' => $basket]); ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="items-tab">
        <?php $this->renderSubview($this->id . '/itemForm', ['basket' => $basket, 'items' => $items]); ?>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.delete').click(function () {
            var itemId = $(this).attr('id');
            if (confirm('Are you sure you want to delete this item?')) {
                $.ajax({
                    type : 'post',
                    url : '<?= App::config('baseUrl') ?>' + '/basket/deleteItem',
                    data: {basketId: '<?= $basket->id ?>', itemId: itemId},
                    success: function (response) {
                        location.reload();
                    }
                });
            }
        });

        $('#add').click(function () {
            var index = $('.item:last').attr('id') != undefined ? (parseInt($('.item:last').attr('id').substr(5)) + 1) : 0;
            $.ajax({
                type: 'post',
                url : '<?= App::config('baseUrl') ?>' + '/basket/addItem/' + index,
                success: function (response) {
                    $('#items').append(response);
                }
            })
        });

        $('#items').on('click', '.delete-item', function () {
            $(this).closest('.item').remove();
        });

        // Hack for openning specific tab from external url
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
        }

        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        })
    });
</script>
