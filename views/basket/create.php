<?php
/**
 * @var models\Basket $basket
 * @var controllers\BasketController $this
 */
?>
<?php $this->renderSubview($this->id . '/form', ['basket' => $basket]) ?>