<?php
use helpers\StringHelper;

/**
 * @var models\Basket $basket
 * @var models\Item[] $items
 * @var controllers\BasketController $this
 * @var string[] $errors
 */
?>

<h2>Items</h2>

<div class="row">
    <div class="col-md-12">
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Weight</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php if ($basket->items): ?>
                <?php foreach ($basket->items as $item): ?>
                    <tr id="itemId-<?= $item->id ?>">
                        <td><?= $item->id ?></td>
                        <td><?= StringHelper::encode($item->type) ?></a></td>
                        <td class="item-weight"><?= StringHelper::encode($item->weight) ?></td>
                        <td><a class="delete" id="<?= $item->id ?>" href="javascript:void(0);"><i class="glyphicon glyphicon-remove"></i></a></td>
                    </tr>
                <?php endforeach ?>
            <?php else: ?>
                <tr><td colspan="3">No data</td></tr>
            <?php endif ?>
            </tbody>
        </table>
    </div>
</div>

<?php if ($basket->hasErrors('content')): ?>
    <p class="text-danger"><?= $basket->getErrors('content')[0] ?></p>
<?php endif ?>

<div class="row">
    <div class="col-md-12">
        <form method="post" class="form-inline">
            <div id="items">
                <?php if (is_array($items)): ?>
                    <?php foreach ($items as $index => $item): ?>
                        <?= $this->renderView($this->id . '/item', ['index' => $index, 'item' => $item]); ?>
                    <?php endforeach ?>
                <?php endif ?>
            </div>

            <input type="hidden" name="tabId" value="1">

            <div class="form-group">
                <button type="button" class="btn btn-primary" id="add"><i class="glyphicon glyphicon-plus"></i> Add Item</button>
                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </form>
    </div>
</div>