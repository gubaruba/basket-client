<?php
use helpers\StringHelper;

/**
 * @var models\Basket[] $baskets
 * @var controllers\BasketController $this
 */
?>

<table class="table table-condensed table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Basket</th>
            <th>Capacity</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php if ($baskets): ?>
        <?php foreach ($baskets as $basket): ?>
            <tr>
                <td><?= StringHelper::encode($basket->id) ?></td>
                <td><a href="<?= App::config('baseUrl') . '/basket/' . $basket->id ?>"><?= StringHelper::encode($basket->name) ?></a></td>
                <td><?= StringHelper::encode($basket->capacity) ?></td>
                <td>
                    <a href="<?= App::config('baseUrl') . '/basket/' . $basket->id . '/update' ?>"><i class="glyphicon glyphicon-pencil"></i></a>
                    <a id="<?= $basket->id ?>" href="javascript:void(0);" class="delete"><i class="glyphicon glyphicon-remove"></i></a>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else: ?>
        <tr><td colspan="3">No data</td></tr>
    <?php endif ?>
    </tbody>
</table>

<script>
    $(document).ready(function () {
        $('.delete').click(function () {
            var id = $(this).attr('id');
            if (confirm('Are you sure you want to delete this item?')) {
                $.ajax({
                    type : 'post',
                    url : '<?= App::config('baseUrl') ?>' + '/basket/' + id + '/delete',
                    success: function (response) {
                        location.reload();
                    }
                });
            }
        });
    });
</script>