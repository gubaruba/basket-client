<?php
use helpers\StringHelper;

/**
 * @var integer $index
 * @var models\Item $item
 * @var controllers\BasketController $this
 */
?>

<div class="item" id="item-<?= $index ?>" style="margin: 10px 0">
    <div class="row">
        <div class="col-md-11"><div class="form-group <?= isset($item->errors->type) ? 'has-error' : '' ?>">
                <label for="name">Type</label>
                <input type="text" class="form-control" id="type" name="item[<?= $index ?>][type]" value="<?= isset($item) ? StringHelper::encode($item->type) : '' ?>">
            </div>

            <div class="form-group <?= isset($item->errors->weight) ? 'has-error' : '' ?>">
                <label for="weight">Weight</label>
                <input type="text" class="form-control" id="weight" name="item[<?= $index ?>][weight]" value="<?= isset($item) ? StringHelper::encode($item->weight) : '' ?>">
            </div>
            <a href="javascript:void(0);" class="delete-item" id="delete-item-<?= $index ?>"><i class="glyphicon glyphicon-remove"></i></a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?php $this->renderSubview('common/errors', ['errors' => isset($item) ? $item->getErrors('type') : '']) ?>
                <?php $this->renderSubview('common/errors', ['errors' => isset($item) ? $item->getErrors('weight') : '']) ?>
            </div>
        </div>
    </div>
</div>