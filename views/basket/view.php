<?php
use helpers\StringHelper;

/**
 * @var models\Basket $basket
 * @var controllers\BasketController $this
 */
?>

<h2>Weight <?= StringHelper::encode($basket->weight) . '/' . StringHelper::encode($basket->capacity) ?></h2>

<div class="progress">
    <div class="progress-bar <?= $basket->getProgressClass() ?>" role="progressbar" style="width: <?= ($basket->weight * 100) / $basket->capacity ?>%"></div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Weight</th>
            </tr>
            </thead>
            <tbody>
            <?php if ($basket->items): ?>
                <?php foreach ($basket->items as $item): ?>
                    <tr>
                        <td><?= $item->id ?></td>
                        <td><?= StringHelper::encode($item->type) ?></a></td>
                        <td><?= StringHelper::encode($item->weight) ?></td>
                    </tr>
                <?php endforeach ?>
            <?php else: ?>
                <tr><td colspan="3">No data</td></tr>
            <?php endif ?>
            </tbody>
        </table>
    </div>
</div>