<?php
return [
    '' => 'basket/index',
    'basket/{id:\d+}' => 'basket/view',
    'basket/create' => 'basket/create',
    'basket/{id:\d+}/update' => 'basket/update',
    'basket/{id:\d+}/delete' => 'basket/delete',
    'basket/deleteItem' => 'basket/deleteItem',
    'basket/addItem/{index:\d+}' => 'basket/addItem',
];