<?php
/**
 * Class App
 *
 * @author Volodymyr Lalykin
 */

/**
 * Application class
 *
 * @author Volodymyr Lalykin
 */
class App
{
    /**
     * @var App singleton of application
     */
    protected static $instance;

    /**
     * @var string configuration of application
     */
    public $config;

    /**
     * Initializes application with configuration array
     *
     * @param string $config
     */
    private function __construct($config)
    {
        $this->config = $config;
    }

    private function __clone()
    {
    }

    /**
     * Returns application singleton
     *
     * @param string[] $config
     * @return App
     */
    public static function getInstance($config)
    {
        if (self::$instance === null) {
            self::$instance = new self($config);
        }
        return self::$instance;
    }

    /**
     * Returns configuration value by parameter name
     *
     * @param string $param
     * @return mixed
     */
    public static function config($param)
    {
        return self::$instance->config[$param];
    }
}