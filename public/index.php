<?php
use helpers\SimpleRouter;

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../App.php');

require(__DIR__ . '/../helpers/SimpleRouter.php');
require(__DIR__ . '/../helpers/StringHelper.php');
require(__DIR__ . '/../helpers/Menu.php');
require(__DIR__ . '/../controllers/Controller.php');
require(__DIR__ . '/../models/Model.php');

$config = require(__DIR__ . '/../config.php');

$routes = require(__DIR__ . '/../routes.php');

$router = new SimpleRouter($routes);

App::getInstance($config);

try {
    $router->handleRequest();
} catch (Exception $e) {
    echo 'Error. Code: ' . $e->getCode() . '. Message: ' . $e->getMessage();
}
