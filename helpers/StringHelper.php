<?php
/**
 * Class StringHelper
 *
 * @author Volodymyr Lalykin
 */
namespace helpers;

/**
 * Class for useful string functions
 * @package helpers
 * @author Volodymyr Lalykin
 */
class StringHelper
{
    /**
     * Encodes user input data into safe string
     *
     * @param string $string
     * @return string encoded string
     */
    public static function encode($string)
    {
        return htmlspecialchars($string, ENT_QUOTES);
    }
}