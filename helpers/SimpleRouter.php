<?php

namespace helpers;

use controllers\Controller;

/**
 * SimpleRouter class contains methods for handling incoming requests
 * @package helpers
 * @author Volodymyr Lalykin
 */
class SimpleRouter
{
    /**
     * @var string controller part of route string
     */
    private $controllerId;

    /**
     * @var string action part of route string
     */
    private $actionId;

    /**
     * @var string[] array where keys are url templates and values are controllerId/actionId routes
     */
    private $routes;

    /**
     * @var string incoming request URI
     */
    private $uri;

    /**
     * @var string[] array where keys are request parameter names and values are appropriate values
     */
    private $params = [];

    /**
     * Initialializes class variables
     *
     * @param string[] $routes
     * @throws \Exception
     */
    public function __construct($routes)
    {
        if (is_array($routes)) {
            $this->routes = $routes;
        } else {
            throw new \Exception('Routes must be an array');
        }

        $this->uri = trim($_SERVER['REQUEST_URI'], '/');
    }

    /**
     * Handles incoming request
     *
     * @throws \Exception
     */
    public function handleRequest()
    {
        $route = $this->getRoute();

        if (!$route) {
            throw new \Exception('Page not found', 404);
        }

        list($this->controllerId, $this->actionId) = explode('/', $route);

        require_once(__DIR__ . '/../controllers/' . $this->getControllerClassName() . '.php');

        $controllerClass = '\\controllers\\' . $this->getControllerClassName();

        if (!class_exists($controllerClass)) {
            throw new \Exception('Class ' . $controllerClass . ' does not exist');
        }

        $controller = new $controllerClass;
        $controller->id = $this->controllerId;
        $controller->actionId = $this->actionId;
        $action = $this->getActionMethodName();


        if (!method_exists($controller, $action)) {
            throw new \Exception('Method ' . $action . ' does not exist');
        }

        if (!$this->run($controller, $action)) {
            throw new \Exception('Invalid params', 400);
        }
    }

    /**
     * Runs controller action with params
     *
     * @param Controller $controller
     * @param string $action the name of an action method
     * @return boolean whether action was successfully run
     */
    public function run($controller, $action)
    {
        $method = new \ReflectionMethod($controller, $action);
        foreach ($method->getParameters() as $param) {
            if (!isset($this->params[$param->name])) {
                if ($param->isDefaultValueAvailable()) {
                    $this->params[$param->name] = $param->getDefaultValue();
                } else {
                    return false;
                }
            }
        }
        $method->invokeArgs($controller, $this->params);
        return true;
    }

    /**
     * Returns controller class name
     *
     * @return string
     */
    public function getControllerClassName()
    {
        return ucfirst($this->controllerId) . 'Controller';
    }

    /**
     * Return action methos name
     *
     * @return string
     */
    public function getActionMethodName()
    {
        return 'action' . ucfirst($this->actionId);
    }

    /**
     * Returns route that corresponds to the current URL
     *
     * @return null|string
     */
    public function getRoute()
    {
        foreach($this->routes as $template => $route) {
            if ($this->checkTemplate($template)) {
                return $route;
            }
        }
        return null;
    }

    /**
     * Checks if current url corresponds to the specific template
     *
     * @param string $template
     * @return boolean
     */
    public function checkTemplate($template)
    {
        $templateSegments = explode('/', $template);
        $uriSegments = explode('/', $this->uri);

        if (count($templateSegments) != count($uriSegments)) {
            return false;
        }

        $this->params = [];
        foreach ($templateSegments as $k => $segment) {
            if ($segment == $uriSegments[$k]) {
                continue;
            } elseif (preg_match('#^{([a-zA-Z]+\w+):(.*)}$#', $segment, $matches)) {
                if (preg_match('#^' . $matches[2] . '$#', $uriSegments[$k])) {
                    $this->params[$matches[1]] = $uriSegments[$k];
                    continue;
                }
            }
            return false;
        }
        return true;
    }
}