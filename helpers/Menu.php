<?php
namespace helpers;

class Menu
{
    private $items = [];

    public function addItem($item)
    {
        $this->items = array_merge($this->items, [$item]);
    }

    public function getItems()
    {
        return $this->items;
    }
}